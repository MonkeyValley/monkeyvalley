import React, {useState} from 'react';
import '../css/navbar.css'
import { Link } from 'react-router-dom'
import logo from "../Assets/logo_monkey_new.svg";
import logoDark from "../Assets/logo_monkey_new_dark.svg";
import Switch from '@material-ui/core/Switch';
import {ThemeProvider, createGlobalStyle} from 'styled-components'
import storage from 'local-storage-fallback'


export default class NavBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            active: "home",
            showMenu:false,
            mode:'light'
        }
        this.hideMenu = this.hideMenu.bind(this);
        

    }

    componentDidMount(){
        const savedTheme = storage.getItem('theme')
        const mod = savedTheme ? JSON.parse(savedTheme): {mode:"light"}
        this.setState({mode:mod.mode})
    }
    hideMenu() {
        this.setState({ showMenu: false });

    }
    handleChange = (event) => {
        this.setState({mode: event.target.checked ? "dark":"light"})
    }
    
    render() {
        return (
            <nav className= { this.state.mode === "dark" ? "nav-bar-dark" : "nav-bar"}>
            
                <Link to="/">
                    <img src={this.state.mode === "dark" ?logoDark: logo} className="img-logo-navbar"/>
                </Link>
                <div className="nav-div-menu" >

                    <button className={this.state.mode === "dark"?"btn-contact-dark":"btn-contact"}>
                        Inicia un proyecto
                        <span className="material-icons">arrow_right_alt</span>
                    </button>
                    <button className={this.state.mode === "dark"?"btn-menu-dropdwon-dark":"btn-menu-dropdwon"} onClick={() => {
                        this.setState({showMenu:!this.state.showMenu})
                    
                    }}> 
                        <span class="material-icons">arrow_drop_down</span>
                    </button>
                </div>
                
                {
                    this.state.showMenu ? 
                    <div className={ this.state.mode==="dark" ? "menu-dark" : "menu"}>
                        <div className="header-menu">
                            Hola, Cristian
                        </div>
                        <button onClick={this.hideMenu}> <span class="material-icons">home</span> Menu item 1 </button>
                        <button onClick={this.hideMenu}> <span class="material-icons">more_vert</span> Menu item 2 </button>
                        <Link className="no-text-decoration" to="/login">
                            <button onClick={this.hideMenu}> <span class="material-icons">account_circle</span>Iniciar sesión</button>
                        </Link>
                        <div className="div-line-divider"/>
                        <div className="div-row-menu div-dark-mode">
                            <div className="div-column-menu div-icon-menu-dark-mode"><span class="material-icons icon-menu">nights_stay</span></div>
                            <div className={ this.state.mode === "dark" ? "div-column-menu div-description-menu-dark-mode-dark" : "div-column-menu div-description-menu-dark-mode"} >Dark mode</div>
                            <div className="div-column-menu div-action-menu-dark-mode">
                                <Switch
                                
                                    checked={this.state.mode === "dark" ? true:false}
                                    onChange={
                                        (e) => {
                                            this.props.onChangeTheme()
                                            this.handleChange(e)
                                        }
                                    }
                                    name="checkedA"
                                    inputProps={{ 'aria-label': 'secondary checkbox' }}
                                />
                            </div>                            
                        </div>
                        
                    </div>
                    :null
                }
            
            </nav>
        );
    }
}
