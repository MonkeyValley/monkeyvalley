import React, {useState} from 'react';
import '../css/navbar.css'
import { Link } from 'react-router-dom'
import logo from "../Assets/logo_monkey_new.svg";
import logoDark from "../Assets/logo_monkey_new_dark.svg";
import Switch from '@material-ui/core/Switch';
import {ThemeProvider, createGlobalStyle} from 'styled-components'
import storage from 'local-storage-fallback'
import { Input } from '@material-ui/core';

const stylesheet = {
    inputStyle:{width: "100%", height: 45, outline: "none", fontSize: 19, border: "none", color: "grey", background: "transparent"},
    label:{fontSize: 12, width:"100%", display:"block"},
    divConteiner:{width:"100%"},
    divShowPassword:{width:"100%", flex:1, flexDirection:"row", display:"flex"},
    divColumn:{flexDirection:"column", flex:1, display:"flex", alignItems:"flex-end"},
    buttonBase:{height:"90%", width:45,background:"transparent", border:"none", outline:"none"},
}

export default class Inputs extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            mode:'light',
            state:true
        }        
    }

    componentDidMount(){
        const savedTheme = storage.getItem('theme')
        const mod = savedTheme ? JSON.parse(savedTheme): {mode:"light"}
        this.setState({mode:mod.mode})
    }
    
    render() {
        return (
            <div>
                {
                    this.props.type==="password" ? 
                        <div style={stylesheet.divConteiner}>
                            <label style={stylesheet.label} className="label-colors" >{this.props.label}</label>
                            <div style={stylesheet.divShowPassword}>
                                <div style={stylesheet.divColumn, {flex:6}}><input style={stylesheet.inputStyle} type= {this.state.state?"password":"text"} placeholder={this.props.hint}/></div>
                                <div style={stylesheet.divColumn, {flex:1, display:"flex", alignItems:"center", justifyContent:"flex-end"}}>
                                    <button style={stylesheet.buttonBase} onClick={()=>{
                                        this.setState({state:!this.state.state})
                                    }}>
                                        <span style={{fontSize:"1.8em"}} className="material-icons label-colors">{this.state.state?"visibility":"visibility_off"}</span>
                                    </button>
                                </div>  
                            </div>
                        </div>

                    :
                        <div>
                            <label style={stylesheet.label} className="label-colors" >{this.props.label}</label>
                            <input style={stylesheet.inputStyle} type="text" placeholder={this.props.hint}/>
                        </div>
                }
            </div>
        );
    }
    
}
