import React, { useState, useEffect } from 'react';
import './css/App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import bg from './Assets/home-bg-01.png'
import NavBar from './components/navapp.js'
import Home from './Views/Home'
import About from './Views/About'
import Login from './Views/Login'
import {ThemeProvider, createGlobalStyle} from 'styled-components'
import storage from 'local-storage-fallback'

const GlobalStyle = createGlobalStyle`
body{
  background-color: ${props => props.theme.mode === 'dark' ? 'rgb(36, 36, 36)':'white'};
  color: ${props => props.theme.mode === 'dark' ? 'gray': '#111'};
  background:${props => props.theme.mode === 'dark' ? 'rgb(36, 36, 36)':'white'};
}
.div-footer{
    background: ${props => props.theme.mode === 'dark' ? 'grey':'#687393'};
}
.svg-footer{
    fill: ${props => props.theme.mode === 'dark' ? 'grey':'#687393'};
    background: ${props => props.theme.mode === 'dark' ? '#4C9AE2':'#EDEDED'}; 
}
.svg-tint{
  width:"10%"
}
.svg-tint path{
  fill: ${props => props.theme.mode === 'dark' ? '#4C9AE2':'#EDEDED'};
}
.div-3-conteiner{
  background: ${props => props.theme.mode === 'dark' ? '#4C9AE2':'#EDEDED'};
}

.label-colors{
  color:${props => props.theme.mode === 'dark' ? 'lightGrey':'darkgrey'};
}
.label-colors-black{
  color:${props => props.theme.mode === 'dark' ? 'lightGrey':'black'};
}

.descriptions-colors{
  color:${props => props.theme.mode === 'dark' ? 'white':'black'};
}
.title-colors{
  color:${props => props.theme.mode === 'dark' ? 'lightGrey':'rgb(36, 36, 36)'};
}
::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: lightgray;
  opacity: 0.8; /* Firefox */
}
:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: lightgray;
}
::-ms-input-placeholder { /* Microsoft Edge */
color: lightgray;
}
`

function getInitialTheme(){
  const savedTheme = storage.getItem('theme')
  return savedTheme ? JSON.parse(savedTheme): {mode:"light"}
}

function App() {

  const [theme, setTheme] = useState(getInitialTheme);
  useEffect( 
    () => { 
      storage.setItem('theme', JSON.stringify(theme));
    }, [theme]); 
    
    return (
       <ThemeProvider theme={theme}>
         <>
         <GlobalStyle/>
        <Router>
          <div className="App" >
            <NavBar onChangeTheme={(event) => {  
                setTheme(theme.mode === 'dark' ? {mode:'light'}: {mode:'dark'})  
              }}/>
            <Switch>
              <Route path='/' exact component={() => <Home theme={theme}/>}/>
              <Route path='/about' component={About} />
              <Route path='/login' component={() => <Login theme={theme}/>} />
            </Switch>
          </div>
        </Router>
        </>
      </ThemeProvider>
    );
}

export default App;