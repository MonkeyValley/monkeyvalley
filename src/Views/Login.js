import React from 'react';
import "../css/login.css"
import Lottie from "react-lottie";
import animationData from "../Animatios/anim_login.json";
import logo from '../Assets/icon_login_light.svg'
import logoWhite from '../Assets/icon_login_dark.svg'
import storage from 'local-storage-fallback'
import Inputs from '../components/Inputs'



class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        mode:'light',
    }
}

  componentDidMount(){
    const savedTheme = storage.getItem('theme')
    const mod = savedTheme ? JSON.parse(savedTheme): {mode:"light"}
    this.setState({mode:mod.mode})
}
  render() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: animationData,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice"
      }
    };
    return (
      <div className="container_login">
          <div className="div-line-navbar"/>
          <div className="conteiner-form">
            <div className="div-animation div-colum">
              <Lottie
                options={defaultOptions}
                width={"80%"} 
                height={"60%"}
              />
            </div>
            <div className="div-colum div-form-conteiner">
                <div className="sub-body-form">
                  <div className="body-form">
                    <div className="header-form"> <img src={this.props.theme.mode === "light" ? logo : logoWhite}/></div>
                    <div className="div-inputs-form">
                      <div className="div-form">
                        <Inputs type="text" hint="hola@monkeyvalley.tech" label="Correo"/>
                        <Inputs type="password" hint="Ingrese su contraseña" label="Contraseña"/>
                      </div>
                    </div>
                    <div className="div-buttons">
                      <div className="div-buttons-column"><button className="btn-base label-colors-black"> ¿Olvidaste tu contraseña?</button></div>
                      <div className="div-buttons-column"><button className="btn-base btn-login"> Entrar  </button></div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
      </div>
    );
  }
}

export default Login;
