import React, { useState, useEffect } from 'react';
import '../css/home.css'
import logo from "../Assets/bg_home_trans.svg";
import logoDark from "../Assets/bg_home_dark.svg";
import Pic1 from "../Assets/img_div_1_transp.png"
import Pic2 from "../Assets/img_div_2_transp.png"
import no_image from "../Assets/ic_no_image.svg"
import planning from "../Assets/bg_planing.svg"
import develop from "../Assets/bg_develop.svg"
import errors from "../Assets/bg_errors.svg"
import svgTop from "../Assets/img_top_planing.svg"
import svgBottom from "../Assets/botto_time_line.svg"
import implementation from "../Assets/bg_implementation.svg"
import storage from 'local-storage-fallback'
import ParticlesBg from 'particles-bg'


class Home extends React.Component {

    
    constructor(props) {
        super(props);
        this.state = {
            mode:'light',
        }
    }
    componentDidMount(){

        const savedTheme = storage.getItem('theme')
        const mod = savedTheme ? JSON.parse(savedTheme): {mode:"light"}
        this.setState({mode:mod.mode})

        console.log("---->", this.props.theme)
    }

    handleClick() {
        alert("hello")
    }

    render() {
        let config = {
            num: [4, 7],
            rps: 0.1,
            radius: [5, 80],
            life: [1.5, 3],
            v: [2, 3],
            tha: [-40, 40],
            alpha: [0.6, 0],
            scale: [.1, 0.4],
            position: "all",
            color: ["random", "#ff0000"],
            cross: "dead",
            emitter: "follow",
            random: 15
          };
          if (Math.random() > 0.85) {
            config = Object.assign(config, {
              onParticleUpdate: (ctx, particle) => {
                ctx.beginPath();
                ctx.rect(
                  particle.p.x,
                  particle.p.y,
                  particle.radius * 2,
                  particle.radius * 2
                );
                ctx.fillStyle = particle.color;
                ctx.fill();
                ctx.closePath();
              }
            });
          }
          let arrayAni = ["color",
        "ball",
        "lines",
        "thick",
        "circle",
        "cobweb",
        "polygon",
        "square",
        "tadpole",
        "fountain",
        "custom"]
        return (
            <div className= {this.props.theme.mode === "light" ? "container" : "container-dark"}>

                <div className='div-container div-1 div-row'>
                    <img className="img-logo" src={this.props.theme.mode === "light" ? logo : logoDark}/> 
                </div>

                <div></div>

                <div className='div-container div-2'>

                    <div className="div-body-row-1-div-2 div-row">
                        <div className="div-column-div-2 div-column div-column-1">
                            <div className='div-title-div-2'>Aplicaciones a la medida de tus necesidades.</div>
                            <div className='div-description-div-2'> Desarrollamos sistemas a la medida siguiendo estrictos estándares de calidad. Nuestras aplicaciones son escalables, robustas y fáciles de usar y trabajamos solo con las principales tecnologías de acuerdo a las necesidades de cada proyecto.</div>
                        </div>
                        <div className="div-column-div-2 div-column div-column-2">
                            <img src={Pic1} style={{width:"70%"}}/> 
                        </div>
                    </div>

                    <div className="div-body-row-2-div-2 div-row">
                        <div className="div-column-div-2 div-column div-column-3">
                            <img src={Pic2} style={{width:"60%"}}/> 
                        </div>
                        <div className="div-column-div-2 div-column div-column-4">
                            <div className='div-title-div-2'>Aplicaciones a la medida de tus necesidades.</div>
                            <div className='div-description-div-2'> Desarrollamos sistemas a la medida siguiendo estrictos estándares de calidad. Nuestras aplicaciones son escalables, robustas y fáciles de usar y trabajamos solo con las principales tecnologías de acuerdo a las necesidades de cada proyecto.</div>
                        </div>
                    </div>
                </div>

                <div className="div-top-svg">
                    {/* <svg  viewBox="0 0 2088 187"  xmlns="http://www.w3.org/2000/svg" > 
                        <path className="svg-tint" d="M0 187C0 139.891 37.2674 101.317 84.3552 99.8989C314.079 92.9784 975.291 72.7879 1596.88 51.2209C2075.24 34.6237 2088 0 2088 0V187H0V187Z" fill="#F8F8F8"/>
                    </svg> */}
                    <svg className="svg-tint"  viewBox="0 0 2089 319" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.3" d="M389 6.76145C199.372 38.3139 1 192.68 1 192.68L0 319H2088L2089 158.646C2089 158.646 1889.47 20.8721 1768.5 58.796C1576.5 118.987 1440 202.805 1172 202.805C904 202.805 690.982 -43.4857 389 6.76145Z" fill="#EDEDED"/>
                        <path opacity="0.66" d="M394.5 41.7588C204.872 73.2987 1.00005 226.749 1.00005 226.749L0 319H2088L2089 193.572C2089 193.572 1894.97 55.8638 1774 93.7727C1582 153.94 1445.5 237.725 1177.5 237.725C909.5 237.725 696.482 -8.46844 394.5 41.7588Z" fill="#EDEDED"/>
                        <path d="M397.5 87.7708C207.872 119.367 0 273.372 0 273.372V319H2088V239.854C2088 239.854 1897.97 101.901 1777 139.877C1585 200.152 1448.5 284.086 1180.5 284.086C912.5 284.085 699.482 37.4542 397.5 87.7708Z" fill="#EDEDED"/>
                    </svg>

                </div>
                <div className="div-3">
                    

                    <div className="div-3-conteiner">
                        <div className="div-3-1">
                            <div className="div-card">
                                <div className="header-card">
                                <img src={planning} className="img-card img-1" />
                                </div>
                                <div className="body-card">
                                    <div className="title-card title-colors">
                                        Planeación
                                    </div>
                                    <div className="description-card label-colors">
                                        Buscamos entender tu visión y necesidades hacerlas nuestras.
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div className="div-3-2">
                            <div className="div-card">
                                <div className="header-card">
                                <img src={develop} className="img-card img-2"/>
                                </div>
                                <div className="body-card">
                                    <div className="title-card title-colors">
                                        Desarrollo
                                    </div>
                                    <div className="description-card label-colors">
                                        Desarrollamos tu sistema siguiendo metodologias ágiles de desarrollo de software.
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div className="div-3-3">
                            <div className="div-card">
                                <div className="header-card">
                                <img src={errors}  className="img-card img-3"/>
                                </div>
                                <div className="body-card">
                                    <div className="title-card title-colors">
                                        Puebas y errores.
                                    </div>
                                    <div className="description-card label-colors">
                                       Le damos el toque de calidad, haciendo pruebas de estrés y testeando al mas minimo detalle para que tu sistema sea como lo esperas.
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div className="div-3-4">
                            <div className="div-card">
                                <div className="header-card">
                                <img src={implementation} className="img-card img-4" />
                                </div>
                                <div className="body-card">
                                    <div className="title-card title-colors">
                                        Implementación
                                    </div>
                                    <div className="description-card label-colors">
                                        Implementamos y distribuimos tu sistema para que llegue a tus clientes.
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                </div>
                <svg className="svg-footer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 99.998">
                    <g id="image" transform="translate(1000 99.998) rotate(180)">
                        <path id="Path_1" data-name="Path 1" d="M473,67.3c-203.9,88.3-263.1-34-320.3,0C66,119.1,0,59.7,0,59.7V0H1000V59.7S937.9,85.8,905.1,89c-32.8,3.3-62.8-12.3-75.8-22.1C806,49.6,745.3,8.7,694.9,4.7S492.4,59,473,67.3Z" opacity="0.33"/>
                        <path id="Path_2" data-name="Path 2" d="M734,67.3c-45.5,0-77.2-23.2-129.1-39.1-28.6-8.7-150.3-10.1-254,39.1s-91.7-34.4-149.2,0C115.7,118.3,0,39.8,0,39.8V0H1000V36.5S971.8,18,907.9,18C810.2,18.1,775.7,67.3,734,67.3Z" opacity="0.66"/>
                        <path id="Path_3" data-name="Path 3" d="M766.1,28.9c-200-57.5-266,65.5-395.1,19.5C242,1.8,242,5.4,184.8,20.6,128,35.8,132.3,44.9,89.9,52.5,28.6,63.7,0,0,0,0H1000s-9.9,40.9-83.6,48.1S829.6,47,766.1,28.9Z"/>
                    </g>
                </svg>
                <div className='div-footer'>

                </div>

                <ParticlesBg config={config}  bg={true} type="polygon" />

            </div>
            
        )
    }
}

export default Home;
